import asyncio
from ruamel.yaml import YAML
import logging

class Server(object):

    def __init__(self):
        self.loop = asyncio.get_event_loop()
        with open('config.yaml') as config:
            self.config = YAML().load(config)
            self.logger = logging.getLogger('server')
            formatter = logging.Formatter(fmt=self.config['logger']['formatter'])
            handler = logging.StreamHandler()
            handler.setFormatter(formatter)
            self.logger.setLevel(self.config['logger']['level'])
            self.logger.addHandler(handler)

    def start(self):
        host = self.config['network']['host']
        port = self.config['network']['port']
        self.logger.info(f'Server started on {host}:{port}\nPress Ctrl+C to stop')
        return asyncio.start_server(self.handle_command, host, port, loop=self.loop)

    async def handle_command(self, reader, writer):
        data = await reader.read(100)
        message = data.decode().strip()
        self.logger.info(f'Recived {message} from {writer.get_extra_info("peername")}')
        response = self.config['commands'].get(message, 'COMMAND NOT FOUND\n').encode('utf-8')
        writer.write(response)
        await writer.drain()
        writer.close()


if __name__ == '__main__':

    server = Server()
    server_coro = server.start()
    server.loop.run_until_complete(server_coro)
    try:
        server.loop.run_forever()
    except KeyboardInterrupt:
        pass
    server_coro.close()
    server.loop.close()
